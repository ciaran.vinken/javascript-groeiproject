import './css/style.scss';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap';

import * as restclient from './js/model/restclient';
import { Brand } from './js/model/Brand';
import { filterTable } from './js/functions/table.js';
import fillHome from './js/functions/fillHome';
import { clearAll, loadForm, loadSearch } from './js/functions/navFunctions';

fillHome();
clearAll();
addEventHandlers();

function addEventHandlers () {
    document.getElementById('nav-home').addEventListener('click', function () {
        clearAll();
        document.getElementById('home').className = 'nav-item active';
        document.getElementById('search').className = 'nav-item';
        document.getElementById('new').className = 'nav-item';
    });
    document.getElementById('nav-new').addEventListener('click', function () {
        loadForm();
        document.getElementById('new').className = 'nav-item active';
        document.getElementById('search').className = 'nav-item';
        document.getElementById('home').className = 'nav-item';
    });
    document.getElementById('nav-search').addEventListener('click', function () {
        loadSearch();
        document.getElementById('search').className = 'nav-item active';
        document.getElementById('new').className = 'nav-item';
        document.getElementById('home').className = 'nav-item';
    });

    document.getElementById('createButton').addEventListener('click', createItem);

    document.getElementById('searchBar').addEventListener('input', filterTable);
    document.getElementById('load').addEventListener('click', fillHome);
}

function createItem () {
    const name = document.getElementById('newName').value;
    const date = document.getElementById('newDate').value;
    const year = document.getElementById('newYear').value;

    const tmpList = restclient.getBrands();

    const newBrand = new Brand();
    newBrand.id = tmpList.length + 1;
    newBrand.brand_name = name;
    newBrand.founding_date = date;
    newBrand.yearsService = year;
    newBrand.logo = 'placeholder.jpg';
    restclient.postBrand(newBrand);
}
