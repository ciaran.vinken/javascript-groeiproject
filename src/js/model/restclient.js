const BASE_URL = 'http://localhost:3000/brands';

export function postBrand (brand) {
    return fetch(BASE_URL, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(brand)
    })
        .then(function (response) {
            if (!response.ok) {
                throw Error(
                    'Unable to POST the brand: ' +
        response.status +
        ' ' +
        response.statusText
                );
            }
            console.log('Brand succesfully POSTed');

            return response.json();
        })
        .catch(function (e) {
            console.error(e);
            throw Error(e);
        });
}

export function deleteBrand (id) {
    return fetch(BASE_URL + '/' + id, {
        method: 'DELETE'
    })
        .then(function (response) {
            if (!response.ok) {
                throw Error(
                    'Unable to DELETE the brand: ' +
                    response.status +
                    ' ' +
                    response.statusText
                );
            }
            return response.json();
        })
        .catch(function (e) {
            console.error(e);
            throw Error(e);
        });
}

export function getBrand (id) {
    return fetch(BASE_URL + '/' + id)
        .then(function (response) {
            if (!response.ok) {
                throw Error(
                    'Unable to GET the brand: ' +
                    response.status +
                    ' ' +
                    response.statusText
                );
            }
            return response.json();
        })
        .catch(function (e) {
            console.error(e);
            throw Error(e);
        });
}

export async function getBrands () {
    return await fetch(BASE_URL)
        .then(async function (response) {
            if (!response.ok) {
                throw Error(
                    'Unable to GET the brands: ' +
                    response.status +
                    ' ' +
                    response.statusText
                );
            }
            return await response.json();
        })
        .catch(function (e) {
            console.error(e);
            throw Error(e);
        });
}
