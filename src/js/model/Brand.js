/* eslint-disable promise/param-names */
import {
} from '../globalVariables.js';
import * as restclient from './restclient';

export class Brand {
    constructor (
        id,
        brandName,
        foundingDate,
        logo,
        yearsService
        // jsonData

    ) {
        this.id = id;
        this.brand_name = brandName;
        this.founding_date = foundingDate;
        this.logo = logo;
        this.yearsService = yearsService;
    }

    getRow () {
        const tableHead = document.getElementById('searchTableHead');
        const tableItems = Array.from(tableHead.children).map(x => x.innerText);
        const row = document.createElement('tr');
        tableItems.forEach(key => {
            const cell = document.createElement('td');
            const div = document.createElement('div');

            if (key === 'logo') {
                const img = document.createElement('img');
                img.src = `http://localhost:3000/${this.logo}`;
                // img.src = restclient.getImg(this);
                cell.appendChild(img);
                cell.classList.add('table-img');
            } else {
                div.textContent = this[key];
                div.classList.add(`table-${key}`);
                cell.appendChild(div);
            }
            row.appendChild(cell);
        });
        row.id = `table-${this.id}`;
        row.className += (' table-row');
        return row;
    }

    getCard () {
        const card = document.createElement('div');
        card.classList.add('brand-card');
        const cardLogo = document.createElement('img');
        cardLogo.classList.add('card-img-top');

        cardLogo.src = `http://localhost:3000/${this.logo}`;
        card.appendChild(cardLogo);
        const cardBody = document.createElement('div');
        cardBody.classList.add('card-body');
        card.appendChild(cardBody);
        const cardName = document.createElement('div');
        cardName.classList.add('card-text');
        cardName.textContent = this.brand_name;

        const cardDate = document.createElement('div');
        cardDate.classList.add('card-text');
        cardDate.textContent = this.founding_date;

        const cardYears = document.createElement('div');
        cardYears.classList.add('card-text');
        cardYears.textContent = this.yearsService;
        cardBody.appendChild(cardName);
        cardBody.appendChild(cardDate);
        cardBody.appendChild(cardYears);

        return card;
    }
}