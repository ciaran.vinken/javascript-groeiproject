import * as restclient from './model/restclient';
import { Brand } from './model/Brand';

let brands;
async function getBrands () {
    if (!brands) {
        let brandsArray = await restclient.getBrands();
        brandsArray = brandsArray.sort((a, b) => a.id < b.id);
        brands = brandsArray.map(b => new Brand(
            b.id,
            b.brand_name,
            b.founding_date,
            b.logo,
            b.yearsService
        ));
    }
    return brands;
}

export {
    getBrands
};
