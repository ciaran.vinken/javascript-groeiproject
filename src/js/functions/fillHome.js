import {
    getBrands
} from '../globalVariables.js';

let start = 0;
async function fillHome () {
    const brands = await getBrands();
    const end = start + 6;
    const firstBrands = brands.slice(start, end);
    start = end;
    if (firstBrands.length <= 0) {
        return;
    }
    const cardContainer = document.getElementById('card-container');
    const card = document.createElement('div');
    card.classList = 'card';
    for (const brand of firstBrands) {
        cardContainer.appendChild(brand.getCard());
    }

    if (end > brands.length) {
        document.getElementById('load').style.display = 'none';
    }
}

export default fillHome;
