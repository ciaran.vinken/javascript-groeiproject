import { fillTable } from './table.js';
import fillHome from './fillHome';

function clearAll () {
    fillHome();
    const el = document.getElementById('new-brand');
    el.style.display = 'none';
    el.classList.add('show');

    const newForm = document.getElementById('searchForm');
    newForm.style.display = 'none';
    newForm.classList.add('show');

    const home = document.getElementById('homeForm');
    home.style.display = 'block';
    home.classList.add('show');
}

function loadForm () {
    const newForm = document.getElementById('new-brand');
    newForm.style.display = 'block';
    newForm.classList.add('show');

    const search = document.getElementById('searchForm');
    search.style.display = 'none';
    search.classList.remove('show');

    const home = document.getElementById('homeForm');
    home.style.display = 'none';
    home.classList.remove('show');
}

function loadSearch () {
    fillTable();
    const newForm = document.getElementById('new-brand');
    newForm.classList.remove('show');
    newForm.style.display = 'none';
    const search = document.getElementById('searchForm');
    search.classList.add('show');

    search.style.display = 'block';
    const home = document.getElementById('homeForm');
    home.classList.remove('show');
    home.style.display = 'none';
}

export {
    clearAll,
    loadForm,
    loadSearch
};
