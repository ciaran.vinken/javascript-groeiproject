import {
    getBrands
} from '../globalVariables';

async function filterTable (e) {
    const filter = e.target.value.toLowerCase();
    let filterTable;
    const brands = await getBrands();
    if (filter !== '') {
        filterTable = brands.filter(b => Object.keys(b).filter(b => b !== 'logo')
            .map(m => b[m].toString().toLowerCase())
            .reduce((c, a) => (a.includes(filter) ? ++c : c), 0) > 0
        );
    } else filterTable = brands;

    brands.forEach(a => {
        const row = document.getElementById(`table-${a.id}`);
        const ids = filterTable.map(a => a.id);
        if (ids.includes(a.id)) {
            row.style.display = 'table-row';
        } else {
            row.style.display = 'none';
        }
    });
}

async function fillTable () {
    const brands = await getBrands();
    const tablebody = document.getElementById('searchTableBody');
    tablebody.innerHTML = '';
    brands.forEach(a =>
        tablebody.appendChild(a.getRow()));
}
export {
    filterTable,
    fillTable
};
